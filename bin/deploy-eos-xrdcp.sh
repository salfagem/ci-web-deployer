#!/bin/bash
set -x

# XROOTD client to copy files to EOS
xrdcp="/usr/bin/xrdcp"
if [ ! -x $xrdcp ]
then
    echo "ERROR: $xrdcp not found"
    exit 1
fi

# Rely in xrootd to do the copy of files to EOS
$xrdcp --force --recursive "$CI_OUTPUT_DIR/" "$EOS_MGM_URL/$EOS_PATH/" 2>&1 >/dev/null
if [ $? -ne 0 ]
then
    echo "ERROR: Failed to copy files from '$CI_OUTPUT_DIR/' to '$EOS_PATH' via xrdcp"
    exit 1
fi
